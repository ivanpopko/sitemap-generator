<?php

namespace Smg\SitemapGenerator\Services\Validates;

use Smg\SitemapGenerator\Services\Exceptions\SitemapInvalidArgumentException;
use Smg\SitemapGenerator\Services\Generator\Generator;

class ArgumentsValidate
{
    /** @var int $maxLocLen максимально допустимия длина URL */
    private static $maxLocLen = 2048;

    /** @var array $changefreqs список допустимых значений */
    private static $changefreqs = [
        'always',
        'hourly',
        'daily',
        'weekly',
        'monthly',
        'yearly',
        'never',
    ];

    /** @var array $priorities список допустимых значений */
    private static $priorities = [
        0.0,
        0.1,
        0.2,
        0.3,
        0.4,
        0.5,
        0.6,
        0.7,
        0.8,
        0.9,
        1.0,
    ];

    /**
     * Проверяем заполнение обязательного тега
     *
     * @param array $arguments
     * @param string $tag
     * @throws SitemapInvalidArgumentException
     */
    private static function emptyRequired(array $arguments, $tag)
    {
        if (!key_exists($tag, $arguments) || empty($arguments[$tag])) {
            throw new SitemapInvalidArgumentException(sprintf("Не указан или пустой тег: %s", $tag));
        }
    }

    /**
     * Проверяем длину URL-страницы (loc)
     *
     * @param array $arguments
     * @throws SitemapInvalidArgumentException
     */
    private static function validateLengthLoc(array $arguments)
    {
        $tag = Generator::LOC_TAG;

        if (key_exists($tag, $arguments) && !(1 <= mb_strlen($arguments[$tag]) && mb_strlen($arguments[$tag]) <= self::$maxLocLen)) {
            throw new SitemapInvalidArgumentException("Невалидная длина URL-адреса страницы. Длина этого значения не должна превышать 2048 символов");
        }
    }

    /**
     * Проверяем Приоритетность (priority)
     *
     * @param array $arguments
     * @throws SitemapInvalidArgumentException
     */
    private static function validatePriorities(array $arguments)
    {
        $tag = Generator::PRIORITY_TAG;

        if (key_exists($tag, $arguments) && !in_array($arguments[$tag], self::$priorities)) {
            throw new SitemapInvalidArgumentException(
                sprintf("Невалидная Приоритетность. Допустимый диапазон значений — от 0,0 до 1,0. Указано:  %.1f", floatval($arguments[$tag]))
            );
        }
    }

    /**
     * Проверяем Вероятную частоту изменения (changefreq)
     *
     * @param array $arguments
     * @throws SitemapInvalidArgumentException
     */
    private static function validateChangefreqs(array $arguments)
    {
        $tag = Generator::CHANGEFREG_TAG;

        if (key_exists($tag, $arguments) && !in_array($arguments[$tag], self::$changefreqs)) {
            throw new SitemapInvalidArgumentException(
                sprintf("Невалидная Вероятная частота изменения. Допустимые значения: %s. Указано:  %s", implode(',', self::$changefreqs), $arguments[$tag])
            );
        }
    }

    /**
     * Проверяем формат Даты (lastmod)
     *
     * @param array $arguments
     * @param string $format
     * @throws SitemapInvalidArgumentException
     */
    private static function validateDate(array $arguments, $format = 'Y-m-d')
    {
        $tag = Generator::LASTMOD_TAG;

        if (key_exists($tag, $arguments)) {
            $d = \DateTime::createFromFormat($format, $arguments[$tag]);

            if (!($d && $d->format($format) === $arguments[$tag])) {
                throw new SitemapInvalidArgumentException(
                    sprintf("Невалидный формат Даты последнего изменения файла. Допустимый формат Y-m-d. Указано:  %s", $arguments[$tag])
                );
            }
        }
    }

    /**
     * @param array $listArguments массив xml-тегов [loc, lastmod, changefreq, priority]
     * @throws SitemapInvalidArgumentException
     */
    public static function validate(array $listArguments)
    {
        foreach ($listArguments as $arguments) {
            self::emptyRequired($arguments, Generator::LOC_TAG);
            self::validateLengthLoc($arguments);
            self::validatePriorities($arguments);
            self::validateChangefreqs($arguments);
            self::validateDate($arguments);
        }
    }
}