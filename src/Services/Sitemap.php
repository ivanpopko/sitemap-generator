<?php

namespace Smg\SitemapGenerator\Services;

use Smg\SitemapGenerator\Services\Exceptions\HandlerSitemapException;
use Smg\SitemapGenerator\Services\Generator\Generator;
use Smg\SitemapGenerator\Services\Generator\GeneratorFactory;
use Smg\SitemapGenerator\Services\Validates\ArgumentsValidate;

class Sitemap
{
    /** @var array $pages список страниц сайта */
    private $pages;

    /** @var string $format тип файла */
    private $format;

    /** @var string $path путь к файлу для сохранения */
    private $path;

    public function __construct(array $pages, $format, $path)
    {
        $this->pages    = $pages;
        $this->format   = $format;
        $this->path     = $path;
    }

    public function build()
    {
        try {
            ArgumentsValidate::validate($this->pages);

            /** @var Generator $generator */
            $generator = (new GeneratorFactory())->getGenerator($this->pages, $this->format, $this->path);

            $generator->create();
        } catch(HandlerSitemapException $ex) {
            echo $ex->customMessage();
        }
    }
}
