<?php

namespace Smg\SitemapGenerator\Services\Exceptions;

/**
 * Класс исключений для ошибок, возникающих при работе с файлом Sitemap
 *
 * Class FileMapException
 * @package Smg\SitemapGenerator\Services\Exceptions
 */
class FileMapException extends HandlerSitemapException
{
    public function customMessage()
    {
        return "FileMap Exception: {$this->getMessage()}";
    }
}