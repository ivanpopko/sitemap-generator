<?php

namespace Smg\SitemapGenerator\Services\Exceptions;

/**
 * Класс исключений для ошибок, возникающих при проверке тегов Sitemap
 *
 * Class SitemapInvalidArgumentException
 * @package Smg\SitemapGenerator\Services\Exceptions
 */
class SitemapInvalidArgumentException extends HandlerSitemapException
{
    public function customMessage()
    {
        return "SiteMapInvalidArgument Exception: {$this->getMessage()}";
    }
}