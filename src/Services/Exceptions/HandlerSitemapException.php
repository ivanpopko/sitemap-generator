<?php

namespace Smg\SitemapGenerator\Services\Exceptions;

/**
 * Ощий класс для каждого созданного класса с типами ошибок
 *
 * Class HandlerSitemapException
 * @package Smg\SitemapGenerator\Services\Exceptions
 */
class HandlerSitemapException extends \Exception
{
    public function customMessage()
    {
        return "HandlerSitemap Exception: {$this->getMessage()}";
    }
}