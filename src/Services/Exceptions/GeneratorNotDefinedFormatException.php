<?php

namespace Smg\SitemapGenerator\Services\Exceptions;

/**
 * Класс исключений для ошибок, возникающих при генерации файлов Sitemap
 *
 * Class GeneratorNotDefinedFormatException
 * @package Smg\SitemapGenerator\Services\Exceptions
 */
class GeneratorNotDefinedFormatException extends HandlerSitemapException
{
    public function customMessage()
    {
        return "Generator Exception: {$this->getMessage()}";
    }
}