<?php

namespace Smg\SitemapGenerator\Services\Files;

use Smg\SitemapGenerator\Services\Exceptions\FileMapException;

class JsonFile extends FileMap
{
    /**
     * @param $text
     * @throws FileMapException
     */
    public function write($text)
    {
        if (!is_dir($this->getPath())) {
            throw new FileMapException("Не удалось записать в файл. Не определена директория.");
        }

        file_put_contents($this->getPathFile(), $text);
    }
}