<?php

namespace Smg\SitemapGenerator\Services\Files;

use Smg\SitemapGenerator\Services\Exceptions\FileMapException;

abstract class FileMap
{
    /** @var \SplFileInfo $splFileInfo */
    private $splFileInfo;

    /** @var array $extensions список допустимых значений */
    private $extensions = [
        'xml',
        'json',
        'csv'
    ];

    /**
     * @param string $filePath
     * @throws FileMapException
     */
    public function __construct($filePath)
    {
        try {
            $this->splFileInfo = new \SplFileInfo($filePath);
        } catch (FileMapException $ex) {
            throw $ex;
        }
    }

    /**
     * @return string
     */
    protected function getExtension()
    {
        return $this->splFileInfo->getExtension();
    }

    /**
     * @return string
     * @throws FileMapException
     */
    protected function getPath()
    {
        if (!in_array($this->getExtension(), $this->extensions)) {
            throw new FileMapException(
                sprintf("Неизвестный формат файла. Допустимые значения: %s.", implode(',', $this->extensions))
            );
        }

        if (empty($this->splFileInfo->getPath())) {
            throw new FileMapException("Не определена директория. Задано пустое значение.");
        }

        if (!file_exists($this->splFileInfo->getPath())) {
            mkdir($this->splFileInfo->getPath(), 0777, true);
        }

        return $this->splFileInfo->getPath();
    }

    /**
     * @return string
     * @throws FileMapException
     */
    protected function getPathFile()
    {
        if (empty($this->getExtension())) {
            throw new FileMapException(sprintf("Не определен файл. Указано: %s", $this->splFileInfo->getFilename()));
        }

        return $this->splFileInfo->getPathname();
    }

    abstract public function write($text);
}