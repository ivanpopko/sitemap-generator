<?php

namespace Smg\SitemapGenerator\Services\Files;

use Smg\SitemapGenerator\Services\Exceptions\FileMapException;

class CsvFile extends FileMap
{
    /**
     * @param $text
     * @throws FileMapException
     */
    public function write($text)
    {
        if (!is_dir($this->getPath())) {
            throw new FileMapException("Не удалось записать в файл. Не определена директория.");
        }

        if (($file = fopen($this->getPathFile(), 'w')) === false) {
            throw new FileMapException("Не удалось открыть файл для записи.");
        }

        fputcsv($file, $text['title'], ';');

        foreach ($text['body'] as $item) {
            fputcsv($file, $item, ';');
        }

        fclose($file);
    }
}