<?php

namespace Smg\SitemapGenerator\Services\Generator;

use Smg\SitemapGenerator\Services\Exceptions\GeneratorNotDefinedFormatException;
use Smg\SitemapGenerator\Services\Files\XmlFile;
use Smg\SitemapGenerator\Services\Files\JsonFile;
use Smg\SitemapGenerator\Services\Files\CsvFile;

class GeneratorFactory
{
    const TYPE_XML  = 'xml';
    const TYPE_JSON = 'json';
    const TYPE_CSV  = 'csv';

    /** @var Generator $generator */
    private $generator;

    /**
     * @param array $pages
     * @param string $format
     * @param string $path
     * @return CsvGenerator|Generator|JsonGenerator|XmlGenerator
     * @throws GeneratorNotDefinedFormatException
     * @throws \Smg\SitemapGenerator\Services\Exceptions\FileMapException|GeneratorNotDefinedFormatException
     */
    public function getGenerator(array $pages, $format, $path)
    {
        $this->generator = null;

        switch ($format) {
            case self::TYPE_XML:
                $this->generator = new XmlGenerator($pages, (new XmlFile($path)));
                break;
            case self::TYPE_JSON:
                $this->generator = new JsonGenerator($pages, (new JsonFile($path)));
                break;
            case self::TYPE_CSV:
                $this->generator = new CsvGenerator($pages, (new CsvFile($path)));
                break;
            default:
                throw new GeneratorNotDefinedFormatException('Формат не определен:' . $format);
        }

        return $this->generator;
    }
}