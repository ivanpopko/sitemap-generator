<?php

namespace Smg\SitemapGenerator\Services\Generator;

use Smg\SitemapGenerator\Services\Files\FileMap;

class JsonGenerator extends Generator
{
    /** @var array $pages */
    private $pages;

    /** @var FileMap $fileMap */
    private $fileMap;

    /**
     * @param array $pages
     * @param FileMap $fileMap
     */
    public function __construct(array $pages, FileMap $fileMap)
    {
        $this->pages    = $pages;
        $this->fileMap  = $fileMap;
    }

    private function generateJson()
    {
        $listPages = [];

        foreach ($this->pages as $key => $page) {
            $listPages[$key][self::LOC_TAG]         = htmlspecialchars($page['loc']);
            $listPages[$key][self::LASTMOD_TAG]     = $page['lastmod'];
            $listPages[$key][self::PRIORITY_TAG]    = $page['priority'];
            $listPages[$key][self::CHANGEFREG_TAG]  = $page['changefreq'];
        }

        return json_encode($listPages);
    }

    public function create()
    {
        $json = $this->generateJson();

        $this->fileMap->write($json);
    }
}