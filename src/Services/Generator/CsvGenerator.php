<?php

namespace Smg\SitemapGenerator\Services\Generator;

use Smg\SitemapGenerator\Services\Files\FileMap;

class CsvGenerator extends Generator
{
    /** @var array $pages */
    private $pages;

    /** @var FileMap $fileMap */
    private $fileMap;

    /**
     * @param array $pages
     * @param FileMap $fileMap
     */
    public function __construct(array $pages, FileMap $fileMap)
    {
        $this->pages    = $pages;
        $this->fileMap  = $fileMap;
    }

    private function setTitle()
    {
        return [
            self::LOC_TAG,
            self::LASTMOD_TAG,
            self::PRIORITY_TAG,
            self::CHANGEFREG_TAG
        ];
    }

    private function setBody()
    {
        $body   = [];

        foreach ($this->pages as $page) {
            $body[] = $page;
        }

        return $body;
    }

    private function generateCsv()
    {
        return [
            'title'   => $this->setTitle(),
            'body'    => $this->setBody()
        ];
    }

    public function create()
    {
        $csv = $this->generateCsv();

        $this->fileMap->write($csv);
    }
}